const express = require('express');
const db = require('../mySqlDb');

const router = express.Router();

router.get('/', async (req, res,next) => {
  try {
    let query = 'SELECT * FROM places';
    let [categories] = await db.getConnection().execute(query);
    console.log(categories);
    return res.send(categories);
  } catch (e) {
    next(e);
  }

});

router.get('/:id', async (req, res, next) => {
  try {
    const [categories] = await db.getConnection().execute('SELECT * FROM places WHERE id = ?', [req.params.id]);
    const category = categories[0];
    if (!category){
      return res.status(404).send({message: 'Not found'});
    }
    return res.send(category);
  } catch (e) {
    next(e);
  }
});

router.post('/', async (req, res, next) => {
  try {
    if (!req.body.name || !req.body.description){
      return res.status(400).send({message: 'Name and description are required'})
    }

    const places = {
      name: req.body.name,
      description: req.body.description,
    };

    let query = 'INSERT INTO places (name, description) VALUES (?, ?)';

    const [results] = await db.getConnection().execute(query, [
      places.name,
      places.description,
    ]);

    const newPlaces = {
      id: results.insertId,
      name: places.name,
      description: places.description,
    };

    return res.send(newPlaces);
  } catch (e) {
    next(e);
  }
});

router.delete('/:id', async (req, res, next) => {
  try {
    const [results] = await db.getConnection().execute('DELETE FROM places WHERE id = ?', [req.params.id]);
    return res.send(results)
  } catch (e) {
    next(e);
  }
})

module.exports = router;