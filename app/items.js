const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const db = require('../mySqlDb');

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },

  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname))
  }
});

const upload = multer({storage});

router.get('/', async (req, res,next) => {
  try {
    let query = 'SELECT * FROM items';

    if (req.query.filter === 'image'){
      query += 'WHERE image IS NOT NULL';
    }

    if (req.query.orderBy === 'date' && req.query.direction === 'desc') {
      query += ' ORDER BY id DESC';
    }

    let [items] = await db.getConnection().execute(query);
    console.log(items);
    return res.send(items);
  } catch (e) {
    next(e);
  }

});

router.get('/:id', async (req, res, next) => {
  try {
    const [items] = await db.getConnection().execute('SELECT * FROM items WHERE id = ?', [req.params.id]);
    const item = items[0];
    if (!item){
      return res.status(404).send({message: 'Not found'});
    }
    return res.send(item);
  } catch (e) {
    next(e);
  }
});

router.post('/', upload.single('image'), async (req, res, next) => {
  try {
    if (!req.body.name){
      return res.status(400).send({message: 'Name and description are required'})
    }

    const office = {
      name: req.body.name,
      description: req.body.description,
      date: req.body.date,
      image: null,
      category_id: req.body.category_id,
      place_id: req.body.place_id
    };

    if (req.file) {
      office.image = req.file.filename;
    }

    let query = 'INSERT INTO items (name, description, date, image, category_id, place_id) VALUES (?, ?, ?, ?, ?, ?)';

    const results = await db.getConnection().execute(query, [
      office.name,
      office.description,
      office.date,
      office.image,
      office.category_id,
      office.place_id,
    ]);

    const newOffice = {
      id: results.insertId,
      name: req.body.name,
      description: req.body.description,
      date: req.body.date,
      image: null,
      category_id: req.body.category_id,
      place_id: req.body.place_id
    };

    return res.send(newOffice);
  } catch (e) {
    next(e);
  }
});

router.delete('/:id', async (req, res, next) => {
  try {
    const [results] = await db.getConnection().execute('DELETE FROM items WHERE id = ?', [req.params.id]);
    return res.send(results)
  } catch (e) {
    next(e);
  }
})

module.exports = router;