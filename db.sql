create schema office collate utf8_general_ci;
use office;

create table categories
(
    id          int auto_increment
        primary key,
    name        varchar(255) null,
    description text         null
);

create table places
(
    id          int auto_increment
        primary key,
    name        varchar(255) null,
    description text         null
);

create table items
(
    id          int auto_increment
        primary key,
    name        varchar(255) not null,
    description text         null,
    date        timestamp    null,
    category_id int          null,
    place_id    int          null,
    image       int          null,
    constraint items_categories_id_fk
        foreign key (category_id) references categories (id),
    constraint items_places_id_fk
        foreign key (place_id) references places (id)
);
insert into categories (id, name, description)
values  (1, 'furniture', 'office furniture'),
        (2, 'Appliances', 'office Appliances');

insert into items (id, name, description, date, category_id, place_id, image)
values  (1, 'notebook', 'office', '1970-01-11 00:00:00', 1, 1, null),
        (2, 'мебель', 'some', '2022-02-12 00:16:42', 2, 1, null);

insert into office.places (id, name, description)
values  (1, ' cabinet of chef', 'floor 4'),
        (2, 'office', 'office 204');